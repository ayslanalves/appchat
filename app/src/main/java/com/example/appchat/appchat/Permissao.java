package com.example.appchat.appchat;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by Ayslan on 07/10/2017.
 */

public class Permissao {

    public static boolean lerContatos(Activity activity){

        int permissaoContatos = 0;

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CONTACTS}, permissaoContatos);
        }

         permissaoContatos = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS);

         return  permissaoContatos == 0;
    }
}
