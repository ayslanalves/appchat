package com.example.appchat.appchat.dominio.entidades;

/**
 * Created by Ayslan on 10/24/2017.
 */

public class Conversa {

    private Contato contato;
    private String ultimaMensagem;
    private boolean visualizada;

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public String getUltimaMensagem() {
        return ultimaMensagem;
    }

    public void setUltimaMensagem(String ultimaMensagem) {
        this.ultimaMensagem = ultimaMensagem;
    }

    public boolean isVisualizada() {
        return visualizada;
    }

    public void setVisualizada(boolean visualizada) {
        this.visualizada = visualizada;
    }

    public Conversa(Contato contato, String ultimaMensagem, boolean visualizada) {

        this.contato = contato;
        this.ultimaMensagem = ultimaMensagem;
        this.visualizada = visualizada;
    }

    public Conversa(){ }
}
