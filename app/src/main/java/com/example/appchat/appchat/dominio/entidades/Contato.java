package com.example.appchat.appchat.dominio.entidades;

/**
 * Created by Ayslan on 07/10/2017.
 */

public class Contato{

    private int id;
    private String nome;
    private String telefone;
    private byte[] foto;

    public Contato() {

        id = 0;
    }

    public Contato(String telefone) {

        this.telefone = telefone;
    }

    public Contato(int id, String telefone, byte[] foto) {

        this.id = id;
        this.telefone = telefone;
        this.foto = foto;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
