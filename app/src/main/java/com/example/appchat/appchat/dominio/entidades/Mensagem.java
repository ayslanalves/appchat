package com.example.appchat.appchat.dominio.entidades;

import java.security.Timestamp;
import java.util.Date;
import java.util.StringTokenizer;

/**
 * Created by Ayslan on 10/21/2017.
 */

public class Mensagem {

    private int id;
    private String origem;
    private boolean visualizada;
    private boolean enviada;
    private String texto;
    protected int IDContato;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrigem() { return origem; }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public boolean isVisualizada() {
        return visualizada;
    }

    public void setVisualizada(boolean visualizada) {
        this.visualizada = visualizada;
    }

    public boolean isEnviada() {
        return enviada;
    }

    public void setEnviada(boolean enviada) {
        this.enviada = enviada;
    }

    public int getIDContato() {
        return IDContato;
    }

    public void setIDContato(int IDContato) {
        this.IDContato = IDContato;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Mensagem(String origem, boolean visualizada, boolean enviada, String texto, int IDContato) {

        this.origem = origem;
        this.visualizada = visualizada;
        this.enviada = enviada;
        this.texto = texto;
        this.IDContato = IDContato;
    }

    public Mensagem(){}
}
