package com.example.appchat.appchat.database;

/**
 * Created by Ayslan on 07/10/2017.
 */

public class ScriptSQL {

    public static String getCreateRegistro(){

        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS REGISTRO ( ");
        sb.append("TELEFONE TEXT NOT NULL);");

        return  sb.toString();
    }

    public static String getCreateContato(){

        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS CONTATO ( ");
        sb.append("_id INTEGER NOT NULL, ");
        sb.append("FOTO BLOB, ");
        sb.append("TELEFONE VARCHAR (14));");

        return  sb.toString();
    }

    public static String getCreateMensagem(){

        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS MENSAGEM2 ( ");
        sb.append("_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ");
        sb.append("CONTATO_ID INT, ");
        sb.append("ORIGEM TEXT, ");
        sb.append("VISUALIZADA BOOLEAN, ");
        sb.append("DATAHORA_ENVIO DATETIME DEFAULT current_timestamp, ");
        sb.append("ENVIADA BOOLEAN, ");
        sb.append("TEXTO TEXT)");

        return  sb.toString();
    }

    public static String dropTabelas(){

        StringBuilder sb = new StringBuilder();
        sb.append("DROP TABLE IF EXISTS REGISTRO;");
        sb.append("DROP TABLE IF EXISTS CONTATO;");
        sb.append("DROP TABLE IF EXISTS MENSAGEM2;");

        return  sb.toString();
    }
}
