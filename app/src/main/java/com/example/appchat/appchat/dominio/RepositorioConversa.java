package com.example.appchat.appchat.dominio;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.appchat.appchat.HelperContato;
import com.example.appchat.appchat.dominio.entidades.Contato;
import com.example.appchat.appchat.dominio.entidades.Conversa;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ayslan on 10/24/2017.
 */

public class RepositorioConversa {

    private SQLiteDatabase conn;

    public RepositorioConversa(SQLiteDatabase conn)
    {
        this.conn = conn;
    }

    public List<Conversa> buscaConversas(Context context){

        List<Conversa> lstConversa = new ArrayList();
        RepositorioContato repositorioContato = new RepositorioContato(conn);

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT CONTATO_ID, MAX(VISUALIZADA) AS VISUALIZADA ");
        sb.append("FROM MENSAGEM2 ");
        sb.append("INNER JOIN CONTATO AS C ON C._id=CONTATO_ID ");
        sb.append("GROUP BY CONTATO_ID ORDER BY MAX(MENSAGEM2._id) DESC");

        Cursor cursor = conn.rawQuery(sb.toString(), null);

        if (cursor.getCount() > 0 )
        {
            cursor.moveToFirst();

            do {

                Contato contato = repositorioContato.buscaContatoById(context, cursor.getInt(cursor.getColumnIndex("CONTATO_ID")));

                Conversa conversa = new Conversa();
                conversa.setContato(contato);
                conversa.setVisualizada(cursor.getInt(cursor.getColumnIndex("VISUALIZADA")) > 0);
                conversa.setUltimaMensagem(buscaUltimaMensagem(contato.getId()));

                lstConversa.add(conversa);

            }while (cursor.moveToNext());

        }

        return  lstConversa;
    }

    private String buscaUltimaMensagem(int IDContato){

        String ultimaMensagem = "";
        Cursor cursor = conn.query("MENSAGEM2", new String[]{ "TEXTO" }, "CONTATO_ID=?", new String[]{ String.valueOf(IDContato) }, null, null, "_id DESC", "1");

        if (cursor.getCount() > 0 ) {

            cursor.moveToFirst();
            ultimaMensagem = cursor.getString(cursor.getColumnIndex("TEXTO"));
        }

        return ultimaMensagem;
    }
}
