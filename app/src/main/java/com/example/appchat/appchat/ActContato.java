package com.example.appchat.appchat;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.appchat.appchat.database.DataBase;
import com.example.appchat.appchat.dominio.RepositorioContato;
import com.example.appchat.appchat.dominio.entidades.Contato;

import java.util.ArrayList;
import java.util.List;

public class ActContato extends AppCompatActivity {

    RecyclerView rvContatos;
    List<Contato> lstContato;
    ContatoAdapter adpContatos;

    private DataBase dataBase;
    private SQLiteDatabase conn;
    private RepositorioContato repositorioContato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contatos);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvContatos = (RecyclerView) findViewById(R.id.rvContatos);
        dataBase = new DataBase(this);
        conn = dataBase.getWritableDatabase();
        repositorioContato = new RepositorioContato(conn);

        lstContato = repositorioContato.buscaContatos(this);
        adpContatos = new ContatoAdapter(lstContato, getApplicationContext());
        rvContatos.setLayoutManager(new LinearLayoutManager(this));
        rvContatos.setAdapter(adpContatos);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        MenuItem searchItem = menu.findItem(R.id.item_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextChange(String newText) {

                List<Contato> lstContatoTemp = new ArrayList<Contato>();

                for (Contato contato: lstContato) {

                    if (contato.getNome().toLowerCase().contains(newText.toLowerCase())) {

                        lstContatoTemp.add(contato);
                    }
                }

                adpContatos = new ContatoAdapter(lstContatoTemp, getApplicationContext());
                rvContatos.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                rvContatos.setAdapter(adpContatos);

                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }
}
