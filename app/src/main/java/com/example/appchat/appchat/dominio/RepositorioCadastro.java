package com.example.appchat.appchat.dominio;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.example.appchat.appchat.dominio.entidades.Contato;

/**
 * Created by Ayslan on 10/25/2017.
 */

public class RepositorioCadastro {

    private SQLiteDatabase conn;

    public RepositorioCadastro(SQLiteDatabase conn)
    {
        this.conn = conn;
    }

    private ContentValues preencheContentValues(String meuNumeroTelefone) {

        ContentValues values = new ContentValues();
        values.put("TELEFONE", meuNumeroTelefone);

        return values;
    }

    public void inserir(String meuNumeroTelefone)
    {
        ContentValues values = preencheContentValues(meuNumeroTelefone);
        conn.insertOrThrow("REGISTRO", null, values);
    }
}
