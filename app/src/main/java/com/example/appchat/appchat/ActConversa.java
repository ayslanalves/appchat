package com.example.appchat.appchat;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.appchat.appchat.database.DataBase;
import com.example.appchat.appchat.dominio.RepositorioContato;
import com.example.appchat.appchat.dominio.RepositorioMensagem;
import com.example.appchat.appchat.dominio.entidades.Contato;
import com.example.appchat.appchat.dominio.entidades.Mensagem;

import java.util.List;

public class ActConversa extends AppCompatActivity {

    RecyclerView rvMensagem;
    List<Mensagem> lstMensagem;
    MensagemAdapter adpMensagem;
    RepositorioMensagem repositorioMensagem;
    int IDContato;

    private DataBase dataBase;
    private SQLiteDatabase conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversa);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle b = getIntent().getExtras();

        if(b != null) {

            dataBase = new DataBase(this);
            conn = dataBase.getWritableDatabase();

            IDContato = b.getInt("CONTATO_ID");
            repositorioMensagem = new RepositorioMensagem(conn);
            lstMensagem = repositorioMensagem.buscaMensagens(this, IDContato);

            rvMensagem = (RecyclerView) findViewById(R.id.rvMensagens);
            adpMensagem = new MensagemAdapter(lstMensagem, getApplicationContext());
            rvMensagem.setLayoutManager(new LinearLayoutManager(this));
            rvMensagem.setAdapter(adpMensagem);
            rvMensagem.scrollToPosition(adpMensagem.getItemCount() - 1);

            RepositorioContato repositorioContato = new RepositorioContato(conn);
            this.getSupportActionBar().setTitle(repositorioContato.buscaContatoById(this, IDContato).getNome());

            ImageView imgFavorite = (ImageView) findViewById(R.id.ic_send);
            imgFavorite.setClickable(true);
            imgFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText editMensagem = (EditText) findViewById(R.id.editMensagem);
                    String textoMensagem = editMensagem.getText().toString();

                    if(textoMensagem.length() > 0) {

                        Mensagem mensagem = new Mensagem("R", true, false, textoMensagem, IDContato);
                        repositorioMensagem.inserir(mensagem);
                        lstMensagem.add(mensagem);
                        adpMensagem.notifyDataSetChanged();
                        rvMensagem.scrollToPosition(adpMensagem.getItemCount() - 1);
                        editMensagem.setText("");
                    }
                }
            });
        }
    }

    @Override
    public boolean onSupportNavigateUp() {

        finish();
        onBackPressed();
        return true;
    }
}
