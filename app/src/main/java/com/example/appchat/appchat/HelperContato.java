package com.example.appchat.appchat;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.text.method.HideReturnsTransformationMethod;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appchat.appchat.database.DataBase;
import com.example.appchat.appchat.dominio.entidades.Contato;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ayslan on 07/10/2017.
 */

public class HelperContato {

    public static String strContatosServidor;

    public static void atualizarContatos(Activity activity){

        getContatosServidor(activity);

        //Log.i("TELEFONES", strContatosServidor);

        List<Contato> lstContatosTelefone = getContatosTelefone(activity);
    }

    public static List<Contato> getContatosTelefone(Activity activity) {

        List<Contato> lstContatosTelefone = new ArrayList();

        ContentResolver cr = activity.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        if ((cur != null ? cur.getCount() : 0) > 0) {

            while (cur != null && cur.moveToNext()) {

                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));

                if (cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {

                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);

                    while (pCur.moveToNext()) {

                        String numeroTelefone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        lstContatosTelefone.add(new Contato(numeroTelefone));
                    }
                    pCur.close();
                }
            }
        }

        if(cur!=null){
            cur.close();
        }

        return lstContatosTelefone;
    }

    public static String getNomeContato(final String numeroTelefone, Context context)
    {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(numeroTelefone));
        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};

        String nomeContato="";
        Cursor cursor=context.getContentResolver().query(uri,projection,null,null,null);

        if (cursor != null) {

            if(cursor.moveToFirst()) {

                nomeContato=cursor.getString(0);
            }
            cursor.close();
        }

        return nomeContato;
    }

    public static void getContatosServidor(final Context context){

        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "http://www.eluselysama.com.br/appchat_getcontatos.php";
        String strContatos;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        strContatosServidor = response;

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);
    }

    public static void teste(String str){

        String tete = str;
    }
}