package com.example.appchat.appchat.dominio;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.appchat.appchat.HelperContato;
import com.example.appchat.appchat.dominio.entidades.Contato;
import com.example.appchat.appchat.dominio.entidades.Mensagem;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Ayslan on 10/24/2017.
 */

public class RepositorioMensagem {

    private SQLiteDatabase conn;

    public RepositorioMensagem(SQLiteDatabase conn)
    {
        this.conn = conn;
    }

    private ContentValues preencheContentValues(Mensagem mensagem) {

        ContentValues values = new ContentValues();
        values.put("CONTATO_ID", mensagem.getIDContato());
        values.put("ORIGEM", mensagem.getOrigem());
        values.put("VISUALIZADA", mensagem.isVisualizada());
        values.put("ENVIADA", mensagem.isEnviada());
        values.put("TEXTO", mensagem.getTexto());

        return values;
    }

    public void inserir(Mensagem mensagem)
    {
        ContentValues values = preencheContentValues(mensagem);
        conn.insertOrThrow("MENSAGEM2", null, values);
    }

    public List<Mensagem> buscaMensagens(Context context, int idContato){

        List<Mensagem> lstMensagem = new ArrayList();

        String[] args = { String.valueOf(idContato) };
        Cursor cursor = conn.query("MENSAGEM2", null, "CONTATO_ID=?", args, null, null, null);

        if (cursor.getCount() > 0 )
        {
            cursor.moveToFirst();

            do {

                Mensagem mensagem = new Mensagem();
                mensagem.setId(cursor.getInt(cursor.getColumnIndex("_id")));
                mensagem.setEnviada(cursor.getInt(cursor.getColumnIndex("ENVIADA")) > 0);
                mensagem.setVisualizada(cursor.getInt(cursor.getColumnIndex("ENVIADA")) > 0);
                mensagem.setOrigem(cursor.getString(cursor.getColumnIndex("ORIGEM")));
                mensagem.setTexto(cursor.getString(cursor.getColumnIndex("TEXTO")));
                mensagem.setIDContato(idContato);

                lstMensagem.add(mensagem);

            }while (cursor.moveToNext());

        }

        return  lstMensagem;
    }
}
