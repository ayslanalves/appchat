package com.example.appchat.appchat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appchat.appchat.database.DataBase;
import com.example.appchat.appchat.dominio.RepositorioCadastro;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

public class ActCadastro extends AppCompatActivity {

    String meuNumeroTelefone;
    private DataBase dataBase;
    private SQLiteDatabase conn;
    private RepositorioCadastro repositorioCadastro;
    EditText etMeuNumero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        dataBase = new DataBase(this);
        conn = dataBase.getWritableDatabase();

        etMeuNumero = (EditText) findViewById(R.id.etMeuNumero);
        etMeuNumero.setInputType(InputType.TYPE_CLASS_NUMBER);

        long numRows = DatabaseUtils.queryNumEntries(conn, "REGISTRO");

        if (numRows > 0) {

            Intent it = new Intent(ActCadastro.this, ActListaConversa.class);
            startActivity(it);

        } else {

            Permissao.lerContatos(this);

            Button btnCadastrar = (Button) findViewById(R.id.btnCadastrar);
            btnCadastrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Util.isNetworkAvailable(getApplicationContext())) {

                        meuNumeroTelefone = etMeuNumero.getText().toString();

                        if (meuNumeroTelefone.length() < 13) {

                            Toast.makeText(getApplicationContext(), "Número de Telefone Inválido!", Toast.LENGTH_LONG).show();

                        } else {

                            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                            String url = "http://www.eluselysama.com.br/appchat_registro.php?TELEFONE=" + meuNumeroTelefone;

                            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {

                                            repositorioCadastro = new RepositorioCadastro(conn);
                                            repositorioCadastro.inserir(meuNumeroTelefone);

                                            OneSignal.sendTag("TELEFONE", meuNumeroTelefone);
                                            OneSignal.startInit(getApplicationContext())
                                                    .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                                                    .unsubscribeWhenNotificationsAreDisabled(true)
                                                    .init();

                                            Intent it = new Intent(ActCadastro.this, ActListaConversa.class);
                                            startActivity(it);
                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    Toast.makeText(getApplicationContext(), "Ocorreu um erro!", Toast.LENGTH_LONG).show();
                                }
                            });

                            queue.add(stringRequest);
                        }
                    }
                    else{

                        Toast.makeText(getApplicationContext(), "Sem conexão com Internet!", Toast.LENGTH_LONG).show();

                    }
                }
            });
        }
    }


    @Override
    protected void onResume() {

        super.onResume();

        dataBase = new DataBase(this);
        conn = dataBase.getWritableDatabase();

        long numRows = DatabaseUtils.queryNumEntries(conn, "REGISTRO");

        if (numRows > 0) {

            Intent it = new Intent(ActCadastro.this, ActListaConversa.class);
            startActivity(it);
        }
    }
}