package com.example.appchat.appchat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appchat.appchat.dominio.entidades.Contato;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ayslan on 07/10/2017.
 */

public class ContatoAdapter extends RecyclerView.Adapter<ContatoAdapter.ContatoViewHolder> {

    private List<Contato> lstContato;
    private Context context;
    View view;

   public ContatoAdapter(List<Contato> lstContato, Context context){

       this.lstContato = lstContato;
       this.context = context;
   }

    @Override
    public ContatoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view = LayoutInflater.from(context).inflate(R.layout.contato, null);
        ContatoViewHolder contactViewHolder = new ContatoViewHolder(view);

        return contactViewHolder;
    }

    @Override
    public void onBindViewHolder(ContatoViewHolder holder, int position) {

        Contato contato = lstContato.get(position);
        holder.nomeContato.setText(contato.getNome());
        holder._id = contato.getId();
   }

    @Override
    public int getItemCount() {
        return lstContato.size();
    }

    public static class ContatoViewHolder extends RecyclerView.ViewHolder{

        int _id;
        TextView nomeContato;

        public ContatoViewHolder(final View itemView) {

            super(itemView);
            nomeContato = (TextView) itemView.findViewById(R.id.nomeContato);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent it = new Intent(itemView.getContext(), ActConversa.class);
                    Bundle b = new Bundle();
                    b.putInt("CONTATO_ID", _id);
                    it.putExtras(b);
                    itemView.getContext().startActivity(it);
                }
            });
        }
    }
}
