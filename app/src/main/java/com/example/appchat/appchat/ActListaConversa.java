package com.example.appchat.appchat;

import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.appchat.appchat.database.DataBase;
import com.example.appchat.appchat.dominio.RepositorioContato;
import com.example.appchat.appchat.dominio.RepositorioConversa;
import com.example.appchat.appchat.dominio.entidades.Contato;
import com.example.appchat.appchat.dominio.entidades.Conversa;
import com.onesignal.OSPermissionObserver;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

import java.util.ArrayList;
import java.util.List;

public class ActListaConversa extends AppCompatActivity {

    RecyclerView rvConversa;
    List<Conversa> lstConversa;
    ConversaAdapter adpConversa;

    private DataBase dataBase;
    private SQLiteDatabase conn;
    private RepositorioConversa repositorioConversa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_conversa);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        dataBase = new DataBase(this);
        conn = dataBase.getWritableDatabase();

        long numRows = DatabaseUtils.queryNumEntries(conn, "REGISTRO");

        HelperContato.atualizarContatos(this);

        if (numRows == 0) {

            Intent it = new Intent(ActListaConversa.this, ActCadastro.class);
            startActivity(it);
            finish();

        } else {

            rvConversa = (RecyclerView) findViewById(R.id.rvConversa);
            repositorioConversa = new RepositorioConversa(conn);

            lstConversa = repositorioConversa.buscaConversas(this);
            adpConversa = new ConversaAdapter(lstConversa, getApplicationContext());
            rvConversa.setLayoutManager(new LinearLayoutManager(this));
            rvConversa.setAdapter(adpConversa);
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        lstConversa = repositorioConversa.buscaConversas(this);
        adpConversa = new ConversaAdapter(lstConversa, getApplicationContext());
        rvConversa.setLayoutManager(new LinearLayoutManager(this));
        rvConversa.setAdapter(adpConversa);
    }

    public void abrirContatos(View view) {

        DataBase database = new DataBase(this);
        SQLiteDatabase conn = database.getWritableDatabase();

        RepositorioConversa repositorioConversa = new RepositorioConversa(conn);
        repositorioConversa.buscaConversas(this);

        boolean permissaoLerContatos = Permissao.lerContatos(this);

        if (permissaoLerContatos) {

            Intent it = new Intent(ActListaConversa.this, ActContato.class);
            startActivity(it);
        }
    }

    public void simularTelefones() {

        try {

            long numRows = DatabaseUtils.queryNumEntries(conn, "CONTATO");

            if (numRows == 0) {

                ArrayList<Contato> lstContato = new ArrayList<Contato>();
                lstContato.add(new Contato(1, "+5562991786380", null));
                lstContato.add(new Contato(2, "+5562992144278", null));
                lstContato.add(new Contato(3, "+5562996637487", null));
                lstContato.add(new Contato(4, "+5562996243600", null));
                lstContato.add(new Contato(5, "+5562999196080", null));
                lstContato.add(new Contato(6, "+5562985401846", null));
                lstContato.add(new Contato(7, "+5562984789841", null));
                lstContato.add(new Contato(8, "+5562984084660", null));
                lstContato.add(new Contato(9, "+5562984831777", null));
                lstContato.add(new Contato(10, "+5562985819323", null));
                lstContato.add(new Contato(11, "+5562984063045", null));

                RepositorioContato repositorioContato = new RepositorioContato(conn);

                for (Contato contato : lstContato) {

                    repositorioContato.inserir(contato);
                }
            }

            Log.i("NUMERO CONTATOS", "TOTAL: " + numRows);
        } catch (Exception ex) {

            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setMessage("ERRO AO INICIAR BANCO DE DADOS: " + ex);
            dlg.setNeutralButton("OK", null);
            dlg.show();
        }

    }
}
