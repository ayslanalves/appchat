package com.example.appchat.appchat.dominio;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.appchat.appchat.ContatoAdapter;
import com.example.appchat.appchat.HelperContato;
import com.example.appchat.appchat.R;
import com.example.appchat.appchat.dominio.entidades.Contato;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Ayslan on 07/10/2017.
 */

public class RepositorioContato {

    private SQLiteDatabase conn;

    public RepositorioContato(SQLiteDatabase conn)
    {
        this.conn = conn;
    }

    private ContentValues preencheContentValues(Contato contato) {

        ContentValues values = new ContentValues();
        values.put("_id", contato.getId());
        values.put("TELEFONE", contato.getTelefone());
        values.put("FOTO", contato.getFoto());

        return values;
    }

    public void excluir(long id)
    {
        conn.delete("CONTATO", " _id =_id ? ", new String[]{ String.valueOf( id ) });
    }

    public void inserir(Contato contato)
    {
        ContentValues values = preencheContentValues(contato);
        conn.insertOrThrow("CONTATO", null, values);
    }

    private List<Contato> buscaContatos(Context context, int idContato)
    {
        List<Contato> lstContato = new ArrayList();

        Cursor cursor;

        if(idContato > -1) {

            cursor = conn.query("CONTATO", null, "_id=" + idContato, null, null, null, null);
        }
        else{

            cursor = conn.query("CONTATO", null, null, null, null, null, null);
        }

        if (cursor.getCount() > 0 )
        {
            cursor.moveToFirst();

            do {

                Contato contato = new Contato();
                contato.setId(cursor.getInt(cursor.getColumnIndex("_id")));
                contato.setTelefone(cursor.getString(cursor.getColumnIndex("TELEFONE")));

                String nomeContato = HelperContato.getNomeContato(contato.getTelefone(), context);
                contato.setNome(nomeContato);

                lstContato.add(contato);

            }while (cursor.moveToNext());

        }

        Collections.sort(lstContato, new Comparator<Contato>() {
            @Override
            public int compare(Contato c1, Contato c2) {
                return c1.getNome().compareTo(c2.getNome());
            }
        });

        return lstContato;
    }

    public Contato buscaContatoById(Context context, int idContato){

        return buscaContatos(context, idContato).get(0);
    }

    public List<Contato> buscaContatos(Context context)
    {
        return buscaContatos(context, -1);
    }
}