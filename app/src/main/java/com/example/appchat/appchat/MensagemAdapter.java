package com.example.appchat.appchat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.appchat.appchat.dominio.entidades.Mensagem;

import org.w3c.dom.Text;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

/**
 * Created by Ayslan on 07/10/2017.
 */

public class MensagemAdapter extends RecyclerView.Adapter<MensagemAdapter.MensageViewHolder> {

    private List<Mensagem> lstMensagem;
    private Context context;
    View view;

   public MensagemAdapter(List<Mensagem> lstMensagem, Context context){

       this.lstMensagem = lstMensagem;
       this.context = context;
   }

    @Override
    public MensageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view = LayoutInflater.from(context).inflate(R.layout.mensagem, null);
        MensageViewHolder contactViewHolder = new MensageViewHolder(view);

        return contactViewHolder;
    }

    @Override
    public void onBindViewHolder(MensageViewHolder holder, int position) {

        Mensagem mensagem = lstMensagem.get(position);
        holder.tvMensagem.setText(mensagem.getTexto());
        holder.origem = mensagem.getOrigem();
        holder._id = mensagem.getId();

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        if(holder.origem.equals("R")) {

            holder.tvMensagem.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.mensagem_recebida));
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.tvMensagem.setLayoutParams(params);
        }
        else{

            holder.tvMensagem.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.mensagem_enviada));
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            holder.tvMensagem.setLayoutParams(params);
        }
    }

    @Override
    public int getItemCount() {
        return lstMensagem.size();
    }

    public static class MensageViewHolder extends RecyclerView.ViewHolder{

        int _id;
        TextView tvMensagem;
        String origem;
        RelativeLayout rlMensagem;

        public MensageViewHolder(final View itemView) {

            super(itemView);
            tvMensagem = (TextView) itemView.findViewById(R.id.mensagem);
            rlMensagem = (RelativeLayout) itemView.findViewById(R.id.rlMensagem);
        }
    }
}
