package com.example.appchat.appchat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.appchat.appchat.dominio.entidades.Conversa;
import com.example.appchat.appchat.dominio.entidades.Mensagem;

import java.util.List;

/**
 * Created by Ayslan on 10/24/2017.
 */

public class ConversaAdapter extends RecyclerView.Adapter<ConversaAdapter.ConversaViewHolder> {

    private List<Conversa> lstConversa;
    private Context context;
    View view;

    public ConversaAdapter(List<Conversa> lstConversa, Context context) {

        this.lstConversa = lstConversa;
        this.context = context;
    }

    @Override
    public ConversaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view = LayoutInflater.from(context).inflate(R.layout.conversa, null);
        ConversaViewHolder conversaViewHolder = new ConversaViewHolder(view);

        return conversaViewHolder;
    }

    @Override
    public void onBindViewHolder(ConversaViewHolder holder, int position) {

        Conversa conversa = lstConversa.get(position);
        holder.tvNomeContato.setText(conversa.getContato().getNome());
        holder.tvUltimaMensagem.setText(conversa.getUltimaMensagem());
        holder._id = conversa.getContato().getId();
    }

    @Override
    public int getItemCount() {

        return lstConversa.size();
    }

    public static class ConversaViewHolder extends RecyclerView.ViewHolder {

        TextView tvNomeContato;
        TextView tvUltimaMensagem;
        int _id;

        public ConversaViewHolder(final View itemView) {

            super(itemView);
            tvNomeContato = (TextView) itemView.findViewById(R.id.nomeContatoConversa);
            tvUltimaMensagem = (TextView) itemView.findViewById(R.id.ultimaMensagem);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    
                    Intent it = new Intent(itemView.getContext(), ActConversa.class);
                    Bundle b = new Bundle();
                    b.putInt("CONTATO_ID", _id);
                    it.putExtras(b);
                    itemView.getContext().startActivity(it);
                }
            });
        }
    }
}
