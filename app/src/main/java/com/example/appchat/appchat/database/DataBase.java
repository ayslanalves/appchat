package com.example.appchat.appchat.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.appchat.appchat.dominio.entidades.Contato;

import java.util.ArrayList;

/**
 * Created by Ayslan on 07/10/2017.
 */

public class DataBase extends SQLiteOpenHelper{

    public DataBase(Context context) {

        super(context, "db_appchat", null, 13);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(ScriptSQL.getCreateRegistro());
        db.execSQL(ScriptSQL.getCreateContato());
        db.execSQL(ScriptSQL.getCreateMensagem());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(ScriptSQL.dropTabelas());
        db.execSQL(ScriptSQL.getCreateRegistro());
        db.execSQL(ScriptSQL.getCreateContato());
        db.execSQL(ScriptSQL.getCreateMensagem());
    }
}
